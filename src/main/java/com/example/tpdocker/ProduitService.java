package com.example.tpdocker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProduitService {
    @Autowired
    private ProduitRepository produitRepository;

    @CacheEvict(cacheManager = "redisCacheManager", cacheNames = "produit", key = "#produit.category", condition = "#produit.category=='SPORT'")
    public Produit save(Produit produit) {
        return produitRepository.save(produit);
    }

    public List<Produit> findAll() {
        return produitRepository.findAll();
    }


}



