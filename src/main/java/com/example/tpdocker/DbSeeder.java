package com.example.tpdocker;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DbSeeder implements CommandLineRunner {
    private ProduitRepository produitRepository;

    public DbSeeder(ProduitRepository produitRepository) {
        this.produitRepository = produitRepository;
    }

    @Override
    public void run(String... args) {

        Produit chaussures = new Produit(
                "CHAUSSURES",
                "SPORT"
        );

        Produit jogging = new Produit(
                "JOGGING",
                "SPORT"
        );

        Produit chemise = new Produit(
                "CHEMISE",
                "LUXE"
        );

        Produit pantalon = new Produit(
                "PANTALON",
                "MODE"
        );

        //delete all produit
        this.produitRepository.deleteAll();

        //add our produit to the database
        List<Produit> produits = Arrays.asList(chaussures, jogging, chemise, pantalon);
        this.produitRepository.saveAll(produits);

        System.out.println("Initialized database");
    }
}
