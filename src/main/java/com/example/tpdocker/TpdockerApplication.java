package com.example.tpdocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;

@SpringBootApplication
public class TpdockerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TpdockerApplication.class, args);
    }

}
