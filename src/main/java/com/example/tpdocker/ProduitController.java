package com.example.tpdocker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/produits")
public class ProduitController {
    public static Logger logger = LoggerFactory.getLogger(ProduitController.class);

    @Autowired
    private ProduitService service;

    @GetMapping("/add/{name}/{category}")
    public List<Produit> add(@PathVariable("name") final String name,
                                @PathVariable("category") final String category) {
        Produit produit =new Produit(name, category);
        service.save(produit);
        return service.findAll();
    }

    @GetMapping ("/")
    public String index() {
        return "TEST";
    }

    @GetMapping ("/all")
    public List<Produit> findAll() {
        return service.findAll();
    }
}
