FROM java:8-jdk-alpine
COPY target/tpdocker-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
EXPOSE 8080
ENTRYPOINT ["java","-Dspring.data.mongodb.uri=mongodb://mymongo/tpdocker","-Dspring.data.redis.uri=redis://myredis","-Djava.security.egd=file:/dev/./urandom", "-jar", "tpdocker-0.0.1-SNAPSHOT.jar"]
